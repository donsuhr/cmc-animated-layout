module.exports = function (grunt, options) {
    return {
        lint: {
            src: [
                './**/*.js',
                '!./app/scripts/vendor/**',
                '!./.tmp/**',
                '!./dist/**',
                '!./node_modules/**',
            ],
        },
    };
};





