var serveStatic = require('serve-static');
var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');

var webpackConfig = require('../../webpack.config');
var bundler = webpack(webpackConfig);

module.exports = function (grunt, options) {
    return {
        options: {
            port: 9005,
            open: true,
            livereload: options.livereloadPort,
            // Change this to '0.0.0.0' to access the server from outside
            hostname: '*',
        },
        livereload: {
            options: {
                middleware: function (connect, middlewareOptions) {
                    return [
                        webpackDevMiddleware(bundler, {
                            // IMPORTANT: dev middleware can't access config, so we should
                            // provide publicPath by ourselves
                            publicPath: webpackConfig.output.publicPath,
                            stats: {
                                colors: true,
                            },
                        }),
                        // bundler should be the same as above
                        webpackHotMiddleware(bundler),
                        function (req, res, next) {
                            if (req.method === 'POST') {
                                req.method = 'GET';
                            }
                            return next();
                        },
                        serveStatic('.tmp'),
                        connect().use('/bower_components', serveStatic('./bower_components')),
                        serveStatic(options.app),
                    ];
                },
            },
        },
        dist: {
            options: {
                port: 9006,
                base: options.dist,
                livereload: false,
                open: 'http://localhost:9006/products/using-campusnexus-to-maximize-enrollment-management/',
                keepalive: true,
            },
        },
    };
};
