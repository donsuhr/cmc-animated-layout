module.exports = function (grunt, options) {
    return {
        options: {
            logConcurrentOutput: true,
            limit: 8,
        },
        dist: [
            'sass',
            'imagemin',
            'svgmin',
            'assemble',
            'copy:dist',
            'webpack:dist',
        ],
        serveSupport: [
            'watch:assemble',
            'watch:sass',
            'watch:js',
            'watch:livereload',
        ],
        serveSupportWithLint: [
            'watch:assemble',
            'watch:sassWithLint',
            'watch:js',
            'watch:livereload',
        ],
    };
};
