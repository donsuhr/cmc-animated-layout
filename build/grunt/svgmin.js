module.exports = function (grunt, options) {
    return {
        dist: {
            files: [
                {
                    expand: true,
                    cwd: options.app + '/images',
                    src: '{,*/}*.svg',
                    dest: options.dist + '/images/pages/products/cmc-animated-layout/',
                },
            ],
        },
    };
};
