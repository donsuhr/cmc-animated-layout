var webpackConfig = require('../../webpack.config.js');

module.exports = function (grunt, options) {
    return {
        options: webpackConfig,
        dist: {
            stats: {
                // Configure the console output
                colors: true,
                modules: true,
                reasons: true,
            },
        },
    };
};
