module.exports = function (grunt, options) {
    return {
        dist: {
            files: [
                {
                    expand: true,
                    dot: true,
                    cwd: '.tmp/images',
                    src: ['*.svg', '*.png'],
                    dest: options.dist + '/images/pages/products/cmc-animated-layout/',
                },
            ],
        },
        html: {
            // dist is moving with replace
            files: [
                {
                    expand: true,
                    dot: true,
                    cwd: '.tmp',
                    src: ['**/*.html'],
                    dest: options.dist  + '/pages/products',
                },
            ],
        },
    };
};
