module.exports = function (grunt, options) {
    return {
        options: {
            flatten: true,
            cwd: options.app + '/assemble-templates/pages',
            // cwd is broke here https://github.com/assemble/assemble/issues/411
            layout: options.app + '/assemble-templates/layouts/default.hbs',
            partials: [options.app + '/assemble-templates/partials/**/*.hbs'],
        },
        pages: {
            files: {
                '.tmp/': [
                    options.app + '/assemble-templates/pages/*.hbs',
                ],
            },
        },
    };
};
