module.exports = function (grunt, options) {
    return {
        options: {
            sourceMap: true,
        },
        dist: {
            files: [
                {
                    expand: true,
                    cwd: '.tmp/styles',
                    src: ['**/*.css'],
                    dest: options.dist + '/styles/pages',
                    ext: '.css',
                },
            ],
        },
    };
};
