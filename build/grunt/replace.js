module.exports = function (grunt, options) {
    return {
        html: {
            src: ['./.tmp/**/*.html'],
            dest: options.dist + '/products/using-campusnexus-to-maximize-enrollment-management/',
            replacements: [
                {
                    from: './images/',
                    to: '/images/pages/products/cmc-animated-layout/',
                },
                {
                    from: './scripts/',
                    to: '/scripts/',
                },
                {
                    from: './styles/',
                    to: '/styles/pages/',
                },
                {
                    from: 'autoplayInProd',
                    to: 'autoplay',
                },
            ],
        },
        css: {
            src: [options.dist + '/styles/**/*.css'],
            overwrite: true,
            replacements: [
                {
                    from: '../images/',
                    to: '/images/pages/products/cmc-animated-layout/',
                },
            ],
        }
    };
};
