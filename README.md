#cmc-animated-layout
Static site generator for animated layout's for the campusmanagement.com products subsite.

[Example](http://www.campusmanagement.com/EN-US/Products/Pages/using-campusnexus-to-maximize-enrollment-management.html)

##Install
After gaining access to the private repo

- `git clone https://bitbucket.org/zorg128/cmc-animated-layout.git`
- `cd cmc-animated-layout`
- `npm install`

project requires grunt-cli `npm install -g grunt-cli`

##Commands

- dev server `grunt serve`
- create sharepoint subsite files `npm run dist`

 