/*eslint-env node */

module.exports = function (grunt) {
    require('time-grunt')(grunt);

    var config = {
        app: 'app',
        dist: 'dist',
        sassOutput: '.tmp/sass-out/',
        svgSpriteOutput: '.tmp/iconizr',
        requireJsOutput: '.require-build',
        livereloadPort: 35749,
        testLiveReload: 35750,
    };

    require('load-grunt-config')(grunt, {
        configPath: require('path').join(process.cwd(), '/build/grunt'),//'./build/grunt',
        data: config,
        jitGrunt: {
            staticMappings: {
                sprite: 'grunt-spritesmith',
                useminPrepare: 'grunt-usemin',
                scsslint: 'grunt-scss-lint',
                replace: 'grunt-text-replace',
                newer: 'grunt-newer',
                svgsprite: 'grunt-svg-sprite', // never worked
            },
        },
    });

    grunt.registerTask('serve', function () {
        grunt.task.run([
            'assemble',
            'sass',
            'autoprefixer',
            'connect:livereload',
            'concurrent:serveSupport',
        ]);
    });

    grunt.registerTask('serveWithLint', function () {
        grunt.task.run([
            'build',
            'connect:livereload',
            'concurrent:serveSupportWithLint',
        ]);
    });

    grunt.registerTask('build', [
        'clean',
        'concurrent:dist',
        'autoprefixer',
        'cssmin',
        'replace',
    ]);

    grunt.registerTask('dist', ['build', 'notify:dist']);

};
