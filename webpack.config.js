/*eslint-env node */

var webpack = require('webpack');
var path = require('path');

var production = process.env.NODE_ENV === 'production';
var appEntry = [path.join(__dirname, 'app/scripts/app.js')];

const plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
];

if (production) {
    plugins.push(
        new webpack.ProvidePlugin({
            'Promise': 'exports?global.Promise!es6-promise',
            'fetch': 'exports?self.fetch!whatwg-fetch',
        }),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.MinChunkSizePlugin({
            minChunkSize: 51200, // ~50kb
        }),
        new webpack.optimize.UglifyJsPlugin({
            mangle: true,
            output: {
                comments: false,
            },
            compress: {
                warnings: false, // Suppress uglification warnings
            },
        }),
        new webpack.DefinePlugin({
            __SERVER__: !production,
            __DEVELOPMENT__: !production,
            __DEVTOOLS__: !production,
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
                BABEL_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        })
    );
} else {
    appEntry.unshift('webpack/hot/dev-server', 'webpack-hot-middleware/client');
    plugins.push(
        new webpack.HotModuleReplacementPlugin()
    );
}

module.exports = {
    debug: !production,
    //profile: true,
    devtool: production ? false :'#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),

    entry: {
        'page--cmc-animated-layout': appEntry,
    },

    resolve: {
        root: [path.join(__dirname, 'app')],
        alias: {
            styles: path.join(__dirname, 'app/styles'),
            images: path.join(__dirname, 'app/images'),
            components: path.join(__dirname, 'app/components'),
            templates: path.join(__dirname, 'app/templates'),
            bower: path.join(__dirname, 'bower_components'),
        },
    },

    output: {
        path: path.join(__dirname, 'dist/'),
        //filename: production ? 'scripts/[name]-[hash].js' : 'scripts/[name].js',
        filename: 'scripts/[name].js',
        //chunkFilename: production ? 'scripts/[name]-[chunkhash].js' : 'scripts/[name].js',
        chunkFilename: '[name].js',
        publicPath: '',
    },

    plugins: plugins,

    module: {
        debug: !production,
        devtool: production ? false : 'eval-source-map',
        loaders: [
            {
                test: /\.js$/,
                loaders: ['monkey-hot', 'babel'],
                include: path.join(__dirname, '/app/'),
            },
        ],
    },
};
