import $ from 'jquery';
import 'jquery.appear';
import debounce from './util/debounce'
import hasCssAnimation from './util/has-css-transition';

function debouncedRecheck() {
    return debounce(function () {
        $.force_appear();
    }, 500);
}

if (hasCssAnimation) {
    $(window).scroll(debouncedRecheck()).resize(debouncedRecheck());
    $(document.body).on('appear', '.animated', function (e, $affected) {
        $(this).addClass('is-visible');
    });
    $('.animated').appear();
    $('.animated:appeared').addClass('is-visible');
} else {
    $('.animated').removeClass('animated');
}
